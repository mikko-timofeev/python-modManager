#!/usr/bin/python3

from sys import argv
from yaml import load
from pprint import pprint

from fetcher import Mod_Info
from versioner import Game_Versions


class Mod_Manager:

    def load_files(config):
        # display configuration
        pprint(config)
        [print('=', end='') for i in range(0, 50)]
        print()

        game_versions = Game_Versions.version_order(
            config['main']['snapshots'],
            config['main']['recent']
        )
        print(game_versions)
        if config['main']['game_ver'] not in game_versions:
            if bool(config['main']['announce']):
                print('"%s" is an invalid game version' % (
                    config['main']['game_ver']
                ))
            return

        # Load resource types list
        all_types = config['types']

        # Load resource list
        all_sources = config['list']

        if all_sources is None:
            # print('Resource list is empty')
            return

        types = {k: v for k, v in all_types.items() if v['enabled']}

        for r_type in types:

            print()
            if r_type not in all_sources:
                print('No resource of type "%s" was found' % (r_type))
                continue
            print('Processing "%s" type' % (r_type))

            for resource in {k: v for k, v in all_sources[r_type].items() if v['enabled']}:
                Mod_Info.get_mod_download_link(
                    mod_url=all_sources[r_type][resource]['link'],
                    game_ver=config['main']['game_ver'],
                    sources=config['sources'],
                    announce=config['main']['announce']
                )
        print('\nDone!')

    def defaults(config):
        # setting defaults unless set already
        if 'announce' not in config['main']:
            config['main']['announce'] = False
        Mod_Manager.load_files(config)

    def configure(configurations=[]):
        config = {}
        for conf in configurations:
            appendable = load(open(conf))
            for el in appendable:
                if el in config:
                    config[el].append(appendable[el])
                else:
                    config[el] = appendable[el]
        Mod_Manager.defaults(config)

    def start():
        if argv[1] is None:
            print('At least one config file must be provided')
            return
        Mod_Manager.configure(argv[1:])


def main():
    Mod_Manager.start()


if __name__ == '__main__':
    main()
