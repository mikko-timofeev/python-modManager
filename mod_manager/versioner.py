#!/usr/bin/python3
# https://minecraft.gamepedia.com/Java_Edition_version_history/Development_versions#Naming
import json
from urllib import request


class Game_Versions:

    MANIFEST = 'https://launchermeta.mojang.com/mc/game/version_manifest.json'

    def version_order(
        snapshots=False,
        limit=10
    ):
        versions = []

        with request.urlopen(Game_Versions.MANIFEST) as url:
            data = json.loads(url.read().decode())

            i = 0
            for version_info in data['versions']:
                if (version_info['type'] == 'snapshot') and not snapshots:
                    continue
                versions.append(version_info['id'])
                i += 1
                if i == limit:
                    break

        return versions


def main():
    print(Game_Versions.version_order())


if __name__ == '__main__':
    main()
